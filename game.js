var player = 0;
const players = [
  {player_name: 'Mael'},
  {player_name: 'Gowther'}
];
var rounds = 0; // Número de rodadas
var winner = false; // Existe vencedor

// Função responsável por alterar o jogador que terá o controle atual, logo, quem irá jogar
function handlePlayer(player) {
  let elPlayer = document.getElementById('player');

  if( player === 0 ) {
    elPlayer.textContent = players[1].player_name;
    elPlayer.className = players[1].player_name;

    return 1;
  } else {
    elPlayer.textContent = players[0].player_name;
    elPlayer.className = players[0].player_name;

    return 0;
  };
};

// Responsável por checar se alguém venceu
function checkVictory(clickedIn) {
  const possibilities = {
    '0': [[0, 1, 2], [0, 4, 8], [0, 3, 6]],
    '1': [[0, 1, 2], [1, 4, 7]],
    '2': [[0, 1, 2], [6, 4, 2], [2, 5, 8]],
    '3': [[3, 4, 5], [0, 3, 6]],
    '4': [[3, 4, 5], [0, 4, 8], [2, 4, 6], [1, 4, 7]],
    '5': [[3, 4, 5], [2, 5, 8]],
    '6': [[6, 7, 8], [2, 4, 6], [0, 3, 6]],
    '7': [[6, 7, 8], [1, 4, 7]],
    '8': [[6, 7, 8], [0, 4, 8], [2, 5, 8]],
  }; // Todas as possibilidades de vitória para cada casa
  squares = document.getElementsByClassName('casa');
  let possibility = possibilities[clickedIn];

  for( let pos = 0; pos < possibility.length; pos++ ) {
    const [a, b, c] = possibility[pos];

    if( squares[a].style.backgroundImage === squares[b].style.backgroundImage && squares[a].style.backgroundImage === squares[c].style.backgroundImage && squares[a].style.backgroundImage != '') {
      winner = true;
      return players[player].player_name;
    };
  };

  return null
};

// Função que permite jogar novamente
function playAgain() {
  rounds = 0;
  player = player === 0 ? 1 : 0;
  winner = false;

  document.getElementById('status').innerHTML = `Next player: <span id="player" class="${players[player].player_name}">${players[player].player_name}</span>`;
  document.getElementById('play-again').style.display = 'none';

  const squares = document.getElementsByClassName('casa');

  for( let pos = 0; pos < squares.length; pos++ ) {
    squares[pos].style.backgroundImage = '';
  };
};

// Função principal, mudando as imagens dos quadrados
function handleSquares(event) {
  square = event.target;

  if( (square.style.backgroundImage === '' || square.style.backgroundImage === null) && winner === false) {
    let path = `./images/${player}.jpg`;
    square.style.backgroundImage = `url('${path}')`;

    rounds++;
    if( rounds >= 5 ) {
      let win = checkVictory(square.id);

      if( win != null ) {
        document.getElementById('status').innerHTML = `Winner: <span id="player" class="${win}">${win}</span>`

        document.getElementById('play-again').style.display = 'inline';

        return;
      };
    };

    if( rounds === 9 && winner === false ) {
      document.getElementById('status').innerHTML = `Empate!`;
      document.getElementById('play-again').style.display = 'inline';

      return;
    };

    player = handlePlayer(player);
  };
};
